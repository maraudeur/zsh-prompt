# zsh-prompt

Simple zsh prompt

## Installation

```bash
git clone https://gitlab.com/maraudeur/zsh-prompt ~/.zsh/plugins
echo "source ~/.zsh/plugins/zsh-prompt/zsh-prompt.plugin.zsh" >> ~/.zshrc
source ~/.zshrc
```

## Screenshot

![screenshot](screenshot.png)

## License

[MIT](LICENSE)
