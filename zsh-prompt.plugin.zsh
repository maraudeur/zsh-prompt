#!/usr/bin/zsh

PROMPT_GIT_STATUS_CLEAN="%F{green}#%f"
PROMPT_GIT_STATUS_MODIFIED="%F{yellow}!?%f"
PROMPT_GIT_STATUS_DIRTY="%F{red}*%f"

PROMPT_GIT_STATUS_PREFIX="("
PROMPT_GIT_STATUS_SUFFIX=")"

# PROMPT_GIT_BRANCH_PREFIX=" "
# PROMPT_GIT_BRANCH_SUFFIX=""

PROMPT_GIT_DIR_PREFIX=""
PROMPT_GIT_DIR_SUFFIX=""

PROMPT_VIRTUALENV_PREFIX=""
PROMPT_VIRTUALENV_SUFFIX=""

PROMPT_CURRENT_DIR_PREFIX=""
PROMPT_CURRENT_DIR_SUFFIX=""

PROMPT_SYMBOL="❯"

SPROMPT="correct %F{red}'%R'%f to %F{green}'%r'%f [%B%Uy%u%bes, %B%Un%u%bo, %B%Ue%u%bdit, %B%Ua%u%bbort]? "

prompt_virtualenv_info() {
  local venv_str=""
  [[ -n "$VIRTUAL_ENV" ]] && {
    venv_str="${PROMPT_VIRTUALENV_PREFIX}${VIRTUAL_ENV##*/}${PROMPT_VIRTUALENV_SUFFIX}"
  }
  echo $venv_str
}

prompt_git_info() {
  local git_status=""
  if $(command git rev-parse --git-dir >/dev/null 2>&1); then
    if $(command git ls-files --other --exclude-standard >/dev/null | grep -q '.'); then
      git_status="$PROMPT_GIT_STATUS_DIRTY"
    else
      if [[ $(command git ls-files --modified >/dev/null 2>&1 | wc -l) -ne 0  ]]; then
        git_status="$PROMPT_GIT_STATUS_MODIFIED"
      else
        git_status="$PROMPT_GIT_STATUS_CLEAN"
      fi
    fi
  fi
  echo "${PROMPT_GIT_STATUS_PREFIX}${git_status}${PROMPT_GIT_STATUS_SUFFIX}"
}

prompt_current_git_dir() {
  local prefix=$(command git rev-parse --show-prefix)
  echo "${vcs_info_msg_1_##*/}${prefix:+"/${prefix%/*}"}"
}

prompt_current_dir() {
  local current_dir="%~"
  [[ -n "${vcs_info_msg_0_}" ]] && {
    current_dir=$(prompt_current_git_dir)
  }
  echo "${PROMPT_CURRENT_DIR_PREFIX}%F{blue}${current_dir}%f${PROMPT_CURRENT_DIR_SUFFIX}"
}

prompt_indicator() {
  echo "%(?.%F{green}${PROMPT_SYMBOL}.%F{red}${PROMPT_SYMBOL})%f"
}

prompt_user() {
  local color="green"
  [[ $UID -eq 0 ]] && {
    color="red"
  }
  echo "%F{${color}}%n%f"
}

prompt_hostname() {
  echo "${SSH_TTY:+"*"}%F{yellow}%m%f"
}

prompt_update() {
  local user=$(prompt_user)
  local host=$(prompt_hostname)
  local directory=$(prompt_current_dir)
  local venvname=$(prompt_virtualenv_info)

  [[ -n "${vcs_info_msg_0_}" ]] && {
    local git_status="$(prompt_git_info)"
    local git_branch="${vcs_info_msg_0_}"
    local git="${git_branch}${git_status}"
    directory="${git} ${directory}"
  }
  local indicator=$(prompt_indicator)
  local newline=$'\n%{\r%}'

  PROMPT="${user} at ${host} in ${directory}"
  PROMPT+="$newline"
  PROMPT+="${venvname}${indicator} "
}

() {
  export VIRTUAL_ENV_DISABLE_PROMPT=1
  setopt prompt_subst

  autoload -Uz vcs_info
  autoload -Uz add-zsh-hook

  zstyle ':vcs_info:*' enable git
  zstyle ':vcs_info:*' max-exports 2
  zstyle ':vcs_info:*:*' formats '%b' '%R'

  add-zsh-hook precmd vcs_info
  add-zsh-hook precmd prompt_update
}

